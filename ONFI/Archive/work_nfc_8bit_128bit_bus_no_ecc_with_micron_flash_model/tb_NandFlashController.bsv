/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- Copyright (c) 2014  Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

////////////////////////////////////////////////////////////////////////////////
// Name of the Module	: Tb to test NFC
// Coded by				: Laxmeesha.S
// The whole test case is done assuming no ECC data is stored in the page. Hence Program and read page operations end exactly at the col addr corresponding to page size 
//  excluding the spare sub-page size . (i.e only 4096B and not 4096B+128B)
////////////////////////////////////////////////////////////////////////////////

package tb_NandFlashController ;

import Vector::*;
import BRAM :: * ;
import InterfaceNandFlashController :: * ;
import NandFlashController :: * ;
import NandFlashFunctBlockTargetTop :: * ;

import NFCTargetConnection :: * ;
import Connectable :: * ;

typedef enum {St1 ,St2 ,St3 ,St4 ,St5 ,St6 ,St7 ,St8 ,St9 ,St10, St11, St12, St13, St14, St15
} State_op deriving( Bits, Eq ) ;

(*synthesize*)
module mkTbNFC ();
	ONFI_Target_Interface_top target_chip <- mkNandFlashTargetTop;
	NFC_Interface nfc <- mkNandFlashController ;

	//mkConnection(nfc.nfc_onfi_interface.dataio0,target_chip.data0);
	//mkConnection(target_chip.data1,nfc.nfc_onfi_interface.dataio1);

	mkConnection ( nfc.nfc_onfi_interface , target_chip ) ;

	Reg#(Bit#(16)) state_count <- mkReg(0) ; // This is to keep track of clk events.
	Reg#(State_op) op_state    <- mkReg(St1) ;
	Reg#(Bit#(32)) max_val     <- mkReg('d1024);
	Reg#(Bit#(32)) data_cnt    <- mkReg(0);
	Reg#(Bit#(32)) error       <- mkReg(0);
	Reg#(Bit#(11)) page_read   <- mkReg(0);
	//Reg#(Bit#(128)) data_A      <- mkReg('hAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA);
	//Reg#(Bit#(128)) data_5      <- mkReg('h55555555555555555555555555555555);
	//Reg#(Bit#(128)) data_A      <- mkReg('hA5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5);
	//Reg#(Bit#(128)) data_5      <- mkReg('hA5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5);
	Reg#(Bit#(128)) data_A      <- mkReg('hFEDCBA98FEDCBA98FEDCBA98FEDCBA98);
	Reg#(Bit#(128)) data_5      <- mkReg('h76543210765432107654321076543210);
	Reg#(Bit#(1)) recv_data  <- mkReg(0);
	Reg#(Bit#(1)) busy_active_low  <- mkReg(1);
	Reg#(Bit#(1)) enable_active_low  <- mkReg(0);
	Reg#(Bit#(1)) disable_active_low <- mkReg(1);
	Reg#(Bit#(1)) enable_active_high <- mkReg(1);
	Reg#(Bit#(1)) disable_active_high <- mkReg(0);
	
	rule rl_wait_for_busy_status_initial (state_count == 0) ;
		if(nfc.nvm_nfc_interface.busy_ == 'h0)
			state_count <= state_count + 1 ; 
	endrule

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 1 full page
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('d0, data_5,'h1) ;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('d0, data_A,'h1) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 2) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			$finish ;
	endrule
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 1  page with col offset
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('d5, data_5,'h1) ; //1 pages with col off
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('d5, data_A,'h1) ; //1 pages with col off
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 2) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			$finish ;
	endrule
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 2 full pages in same lun
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('h0, data_5,'h2) ; //2 pages in same Lun
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('h0, data_A,'h2) ; //2 pages in same Lun
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h0, data_5,'h2) ;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h0, data_5,'h2) ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St3 ;
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 3) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			$finish ;
	endrule
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 2 full pages in different lun
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('h1000, data_5,'h2) ;//2 pages in diff Lun
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('h1000, data_A,'h2) ;//2 pages in diff Lun
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h1000, data_5,'h2) ;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h1000, data_5,'h2) ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St3 ;
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 3) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			$finish ;
	endrule
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 2 pages with offset in same lun
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('d5, data_5,'h2) ; //2 pages with col off
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('d5, data_A,'h2) ; //2 pages with col off
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('d5, data_5,'h2) ;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('d5, data_5,'h2) ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
				
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 3) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			$finish ;
	endrule
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 3 full pages in (2 in lun0 + 1 in lun1)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('h0, data_5,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('h0, data_A,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h0, data_5,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h0, data_A,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_3_page_write_req (state_count == 3) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h0, data_5,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h0, data_A,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 4) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			$finish ;
	endrule

*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 3  pages in (2 in lun0 + 1 in lun1) with col offset
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('h7, data_5,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('h7, data_A,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h7, data_5,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h7, data_A,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St3 ;
			      end
		endcase
	endrule
	
	rule rl_send_3_page_write_req (state_count == 3) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h7, data_5,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h7, data_A,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St3 ;
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 4) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			$finish ;
	endrule
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 3 full pages in (2 in lun0 + 1 in lun1)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('h1000, data_5,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('h1000, data_A,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h1000, data_5,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h1000, data_A,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St3 ;
			      end
		endcase
	endrule
	
	rule rl_send_3_page_write_req (state_count == 3) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h1000, data_5,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h1000, data_A,'h3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
				
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 4) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			$finish ;
	endrule
*/	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 4 full pages in (2 in lun0 + 2 in lun1)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_3_page_write_req (state_count == 3) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_4_page_write_req (state_count == 4) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 5) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			$finish ;
	endrule
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 4 full pages in (1 in lun1 + 2 in lun0 + 1 in lun1)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('h3000, data_5,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('h3000, data_A,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h3000, data_5,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h3000, data_A,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_3_page_write_req (state_count == 3) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h3000, data_5,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h3000, data_A,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_4_page_write_req (state_count == 4) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h3000, data_5,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h3000, data_A,'h4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 5) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			$finish ;
	endrule
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 5 full pages in (2 in lun0 + 2 in lun1 + 1 in lun0)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('h0, data_5,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('h0, data_A,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h0, data_5,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h0, data_A,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_3_page_write_req (state_count == 3) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h0, data_5,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h0, data_A,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_4_page_write_req (state_count == 4) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h0, data_5,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h0, data_A,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_5_page_write_req (state_count == 5) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h0, data_5,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h0, data_A,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	/*rule rl_send_6_page_write_req (state_count == 6) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h0, data_5,'h6) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h0, data_A,'h6) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule*/
	
	rule rl_check_1_page_write_req_complete (state_count == 6) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			$finish ;
	endrule

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 5 full pages in (1 in lun0 + 2 in lun1 + 2 in lun0)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('h5000, data_5,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('h5000, data_A,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h5000, data_5,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h5000, data_A,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_3_page_write_req (state_count == 3) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h5000, data_5,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h5000, data_A,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_4_page_write_req (state_count == 4) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h5000, data_5,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h5000, data_A,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_5_page_write_req (state_count == 5) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h5000, data_5,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h5000, data_A,'h5) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 6) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			$finish ;
	endrule

*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 8 full pages in (2 in lun0 + 2 in lun1 + 2 in lun0 + ...)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_3_page_write_req (state_count == 3) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_4_page_write_req (state_count == 4) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_5_page_write_req (state_count == 5) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_6_page_write_req (state_count == 6) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_7_page_write_req (state_count == 7) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_8_page_write_req (state_count == 8) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'h8) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 9) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			$finish ;
	endrule
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 13 full pages in (2 in lun1 + 2 in lun0 + 2 in lun1 + ...)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_3_page_write_req (state_count == 3) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_4_page_write_req (state_count == 4) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_5_page_write_req (state_count == 5) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_6_page_write_req (state_count == 6) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_7_page_write_req (state_count == 7) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_8_page_write_req (state_count == 8) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_9_page_write_req (state_count == 9) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_10_page_write_req (state_count == 10) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_11_page_write_req (state_count == 11) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_12_page_write_req (state_count == 12) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_13_page_write_req (state_count == 13) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 14) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			$finish ;
	endrule
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Read 1 full page
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_read_req (state_count == 1) ;
	      nfc.nvm_nfc_interface._request_data('h0,'d1) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 2) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != 'hFF)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != 'hFF) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
		
	rule rl_end_read (page_read == 'd1) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule	
*/	

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Read 1 page with offset
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_read_req (state_count == 1) ;
	      nfc.nvm_nfc_interface._request_data('h06,'d1) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 2) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
		
	rule rl_end_read (page_read == 'd1) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule	
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Read 2 page with offset
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_read_req (state_count == 1) ;
	      nfc.nvm_nfc_interface._request_data('h06,'d2) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 2) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
		
	rule rl_end_read (page_read == 'd2) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule	
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Read 2 pages (1 in lun1 + 1 in lun0)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_read_req (state_count == 1) ;
	      nfc.nvm_nfc_interface._request_data('h3000,'d2) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 2) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
		
	rule rl_end_read (page_read == 'd2) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule	
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Read 3 pages (2 in lun1 + 1 in lun0)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_read_req (state_count == 1) ;
	      nfc.nvm_nfc_interface._request_data('h2000,'d3) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 2) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
		
	rule rl_end_read (page_read == 'd3) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule	
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Read 3 pages (1 in lun0 + 2 in lun1)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_read_req (state_count == 1) ;
	      nfc.nvm_nfc_interface._request_data('h00,'d3) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 2) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
		
	rule rl_end_read (page_read == 'd3) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule	
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Read 4 pages (2 in lun0 + 2 in lun1)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_read_req (state_count == 1) ;
	      nfc.nvm_nfc_interface._request_data('h00,'d4) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 2) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != 'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != 'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
		
	rule rl_end_read (page_read == 'd4) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule	
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Read 4 pages (1 in lun1 + 2 in lun0 + 1 in lun1)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_read_req (state_count == 1) ;
	      nfc.nvm_nfc_interface._request_data('h23000,'d4) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 2) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
		
	rule rl_end_read (page_read == 'd4) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule	
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Read 5 pages (1 in lun0 + 2 in lun1 + 2 in lun0)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_read_req (state_count == 1) ;
	      nfc.nvm_nfc_interface._request_data('h9000,'d5) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 2) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
		
	rule rl_end_read (page_read == 'd5) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule	
*/	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Read 5 pages (2 in lun0 + 2 in lun1 + 1 in lun0)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_read_req (state_count == 1) ;
	      nfc.nvm_nfc_interface._request_data('h0,'d5) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 2) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != 'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != 'hFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
		
	rule rl_end_read (page_read == 'd5) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule	
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Read 8 pages (2 in lun1 + 2 in lun0 + 2 in lun1 ...)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_read_req (state_count == 1) ;
	      nfc.nvm_nfc_interface._request_data('hA000,'d8) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 2) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
		
	rule rl_end_read (page_read == 'd8) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule			
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Read 8 pages (1 in lun0 + 2 in lun1 + 2 in lun0 ...)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_read_req (state_count == 1) ;
	      nfc.nvm_nfc_interface._request_data('h00,'d8) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 2) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
		
	rule rl_end_read (page_read == 'd8) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule		
*/	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Read 13 pages (1 in lun1 + 2 in lun0 + 2 in lun1 ...)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_read_req (state_count == 1) ;
	      nfc.nvm_nfc_interface._request_data('hF000,'d13) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 2) ;
		case (op_state)
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
		
	rule rl_end_read (page_read == 'd13) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Erase block 1 with lun1 address
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_erase_req (state_count == 1) ;
		//nfc.nvm_nfc_interface._request_erase('h31000) ; 
		nfc.nvm_nfc_interface._request_erase('h00) ; 
		nfc.nvm_nfc_interface._enable('h0) ;
		state_count <= state_count + 'h1 ;
	endrule
	
	rule rl_monitor (state_count == 2) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.erase_success_ == 1'b1 || nfc.nvm_nfc_interface.erase_fail_ == 1'b1)
			$finish ;
	endrule	
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Erase block 0 with lun0 plane0 address
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_erase_req (state_count == 1) ;
		nfc.nvm_nfc_interface._request_erase('h00) ; 
		nfc.nvm_nfc_interface._enable('h0) ;
		state_count <= state_count + 'h1 ;
	endrule
	
	rule rl_monitor (state_count == 2) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.erase_success_ == 1'b1 || nfc.nvm_nfc_interface.erase_fail_ == 1'b1)
			$finish ;
	endrule	
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Erase block 3 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_erase_req (state_count == 1) ;
		nfc.nvm_nfc_interface._request_erase('h60000) ; 
		nfc.nvm_nfc_interface._enable('h0) ;
		state_count <= state_count + 'h1 ;
	endrule
	
	rule rl_monitor (state_count == 2) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.erase_success_ == 1'b1 || nfc.nvm_nfc_interface.erase_fail_ == 1'b1)
			$finish ;
	endrule	
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 2 full pages then read and compare
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'d2) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'d2) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'d2) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'d2) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 3) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			state_count <= state_count + 1 ; 
	endrule	
	
	rule rl_send_read_req (state_count == 4) ;
	      nfc.nvm_nfc_interface._request_data('h00,'d2) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 5) ;
		case (op_state)
			 St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
	
	rule rl_end_read (page_read == 'd2) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 3 full pages then read and compare
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'d3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'d3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'d3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'d3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_3_page_write_req (state_count == 3) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'d3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d3) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	
	rule rl_check_1_page_write_req_complete (state_count == 4) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			state_count <= state_count + 1 ; 
	endrule	
	
	rule rl_send_read_req (state_count == 5) ;
	      nfc.nvm_nfc_interface._request_data('h00,'d4) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 6) ;
		case (op_state)
			 St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
	
		//comment
	rule rl_end_read (page_read == 'd3) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 4 full pages then read and compare
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'d4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'d4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'d4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'d4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_3_page_write_req (state_count == 3) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'d4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_4_page_write_req (state_count == 4) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h00, data_5,'d4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h00, data_A,'d4) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	
	rule rl_check_1_page_write_req_complete (state_count == 5) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			state_count <= state_count + 1 ; 
	endrule	
	
	rule rl_send_read_req (state_count == 6) ;
	      nfc.nvm_nfc_interface._request_data('h00,'d4) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 7) ;
		case (op_state)
			 St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
	
		//comment
	rule rl_end_read (page_read == 'd4) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 13 full pages in (2 in lun1 + 2 in lun0 + 2 in lun1 + ...) then read and compare
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_3_page_write_req (state_count == 3) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_4_page_write_req (state_count == 4) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_5_page_write_req (state_count == 5) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_6_page_write_req (state_count == 6) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_7_page_write_req (state_count == 7) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_8_page_write_req (state_count == 8) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_9_page_write_req (state_count == 9) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_10_page_write_req (state_count == 10) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_11_page_write_req (state_count == 11) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_12_page_write_req (state_count == 12) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_13_page_write_req (state_count == 13) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 14) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			state_count <= state_count + 1 ; 
	endrule	
	
	rule rl_send_read_req (state_count == 15) ;
	      nfc.nvm_nfc_interface._request_data('hA000,'d13) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 16) ;
		case (op_state)
			 St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
	
		
	rule rl_end_read (page_read == 'd13) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Program 13 full pages in (2 in lun1 + 2 in lun0 + 2 in lun1 + ...) in chip0 then read and compare, then repeat for 7 pages in chip1
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/*
	rule rl_send_1_page_write_req (state_count == 1) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req (state_count == 2) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_3_page_write_req (state_count == 3) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_4_page_write_req (state_count == 4) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_5_page_write_req (state_count == 5) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_6_page_write_req (state_count == 6) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_7_page_write_req (state_count == 7) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_8_page_write_req (state_count == 8) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_9_page_write_req (state_count == 9) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_10_page_write_req (state_count == 10) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_11_page_write_req (state_count == 11) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_12_page_write_req (state_count == 12) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_13_page_write_req (state_count == 13) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('hA000, data_5,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('hA000, data_A,'d13) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_check_1_page_write_req_complete (state_count == 14) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			state_count <= state_count + 1 ; 
	endrule	
	
	rule rl_send_read_req (state_count == 15) ;
	      nfc.nvm_nfc_interface._request_data('hA000,'d13) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	
	rule rl_monitor_interrupt ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 16) ;
		case (op_state)
			 St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_1_page_write_req_c1 (state_count == 16 && page_read == 'd13 ) ;
		 case (op_state)
			
			St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._write('h90000, data_5,'d7) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St2 : begin
				nfc.nvm_nfc_interface._write('h90000, data_A,'d7) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					page_read <= 'h0 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
				end
				op_state <= St1 ;
			      end
		endcase
	endrule
	
	rule rl_send_2_page_write_req_c1 (state_count == 17) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h90000, data_5,'d7) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h90000, data_A,'d7) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_3_page_write_req_c1 (state_count == 18) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h90000, data_5,'d7) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h90000, data_A,'d7) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_4_page_write_req_c1 (state_count == 19) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h90000, data_5,'d7) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h90000, data_A,'d7) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_5_page_write_req_c1 (state_count == 20) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h90000, data_5,'d7) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h90000, data_A,'d7) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_6_page_write_req_c1 (state_count == 21) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h90000, data_5,'d7) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h90000, data_A,'d7) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	rule rl_send_7_page_write_req_c1 (state_count == 22) ;
		 case (op_state)
			//Wait 1 cycle and then poll busy flag.
			St1 : begin
					op_state <= St2 ;
			      end
			St2 : begin
				if(nfc.nvm_nfc_interface.busy_ == 'h0)
					op_state <= St3 ;
			      end
			St3 : begin
				op_state <= St4 ;
				nfc.nvm_nfc_interface._write('h90000, data_5,'d7) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
			      end
			St4 : begin
				nfc.nvm_nfc_interface._write('h90000, data_A,'d7) ; 
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					state_count <= state_count + 1 ; 
					data_cnt <= 'h0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St3 ;
				end
			      end
		endcase
	endrule
	
	
	rule rl_check_1_page_write_req_complete_c1 (state_count == 23) ;
		nfc.nvm_nfc_interface._enable('h1) ;
		if(nfc.nvm_nfc_interface.write_success_ == 1'b1)
			state_count <= state_count + 1 ; 
	endrule	
	
	rule rl_send_read_req_c1 (state_count == 24) ;
	      nfc.nvm_nfc_interface._request_data('h90000,'d7) ; 
	      nfc.nvm_nfc_interface._enable('h0) ;
	      state_count <= state_count + 1 ; 
	endrule
	
	
	rule rl_monitor_interrupt_c1 ((nfc.nvm_nfc_interface.interrupt_ == 1'b1 || recv_data == 1'b1)&& state_count == 25) ;
		case (op_state)
			 St1 : begin
				op_state <= St2 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				recv_data <= 1'b1 ;
			      end
			St2 : begin
				op_state <= St3 ;
				if(nfc.nvm_nfc_interface._get_data_ != data_5)  
					error <= error + 'h1;
				nfc.nvm_nfc_interface._enable('h0) ;
				data_cnt <= data_cnt+'h1 ;
				recv_data <= 1'b1 ;
			      end
			St3 : begin
				if(nfc.nvm_nfc_interface._get_data_ != data_A) 
					error <= error + 'h1 ;
				nfc.nvm_nfc_interface._enable('h0) ;
				if(data_cnt == (max_val-1))
				begin
					data_cnt <= 'h0 ;
					page_read <= page_read + 'h1 ;
					recv_data <= 1'b0 ;
					op_state <= St1 ;
				end
				else
				begin
					data_cnt <= data_cnt+'h1 ;
					op_state <= St2 ;
				end
			      end
		endcase
	endrule
		
	rule rl_end_read (page_read == 'd7 && state_count == 25) ;
		if(error > 0)
			$display("read error present");
		$finish ;
	endrule
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////// Query bad block from block 1 to block 6
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
/* 
	rule rl_send_query (state_count == 1) ;
		nfc.nvm_nfc_interface._query_bad_block('h100000006) ; //offset 6 (of 32 bits) addr 1 (32 bits)
		nfc.nvm_nfc_interface._enable('h0) ;
		state_count <= state_count + 1 ; 
	endrule
	
	rule rl_monitor_bb (state_count == 2 && (nfc.nvm_nfc_interface.interrupt_ == 1'b1)) ;
		state_count <= state_count + 1 ; 
	endrule	 
	
	rule rl_finish (state_count >= 3 ) ;
		if(state_count == 10)
			$finish;
		else
			state_count <= state_count + 1 ; 
	endrule
*/
endmodule
endpackage
