package NFCECCencoderConnection ;

import Connectable :: * ;

import InterfaceNandFlashController :: * ;
import InterfaceECCencoder :: * ;

instance Connectable #( NFC_ECCencoderInterface , ECCencoder_NFCInterface ) ;
	module mkConnection #( NFC_ECCencoderInterface nfc_ecc_enc_ifc , ECCencoder_NFCInterface ecc_enc_nfc_ifc ) ( Empty ) ;

		 (* no_implicit_conditions, fire_when_enabled *)
		rule rl_data_from_ecc ;
			nfc_ecc_enc_ifc._data_from_ecc_encoder(ecc_enc_nfc_ifc.data_to_nfc) ;
		endrule
		
		rule data_to_ecc ;
			ecc_enc_nfc_ifc._data_from_nfc(nfc_ecc_enc_ifc.data_to_ecc_encoder);
		endrule
		
		rule rl_ce ;
			ecc_enc_nfc_ifc._ecc_encoder_ce(nfc_ecc_enc_ifc.ecc_encoder_ce);
		endrule
		
		rule rl_start ;
			ecc_enc_nfc_ifc._ecc_encoder_start(nfc_ecc_enc_ifc.ecc_encoder_start);
		endrule
		
	endmodule
endinstance
endpackage
