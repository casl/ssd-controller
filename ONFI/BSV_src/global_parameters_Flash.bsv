`define COLUMN_WIDTH 12             //Corresponds to No. of bytes needed in a page. Eg : 2^12 = 4kB  
`define PAGE_WIDTH 3                //Corresponds to No. of pages needed in a block. Eg : 2^6 = 64 Pages 
`define BLOCK_WIDTH 2               //Corresponds to No. of blocks needed in a PLANE(not in a LUN). Eg : 2^10 = 1024 blocks per plane 
`define PLANE_WIDTH 1               //Corresponds to No. of planes needed in a LUN. This s FIXED to 2^1 = 2.
`define LUN_WIDTH 1                 //Corresponds to No. of LUNS needed in a target chip. This s FIXED to 2^1 = 2.
`define TOTAL_PLANE 4               //#LUNS * #planes. Fixed
`define VALID_COL_ADDR 4159         //(2^COLUMN_WIDTH-1)+SPARE_BYTES_AREA(Say 64B)
`define WDC 128                      //NVMe - NFC Bus width
`define TOTAL_CHIPS 2               //Total chips per NFC

`define VALID_SPARE_AREA 63         // Size of spare area in Bytes - 1, (Say 0 to 63B total of 64B)
`define SECTOR_SPARE_SIZE 8         // Size of spare area for each sector.  (VALID_SPARE_AREA+1)*512B/(2^COLUMN_WIDTH)

`define FIFO_ROWS 255              //Page_size/WDC -1               
`define LBPR 4                      //Log Bytes Per Row. log(WDC/8).

//Based on above value comment out suitable things below.
//`define COLUMN_WIDTH_LT_8
//`define COLUMN_WIDTH_E_8
`define COLUMN_WIDTH_GT_8

`define PAGE_WIDTH_LT_8
//`define PAGE_WIDTH_E_8
//`define PAGE_WIDTH_GT_8

`define PAGE_PLANE_WIDTH_LT_8
//`define PAGE_PLANE_WIDTH_E_8
//`define PAGE_PLANE_WIDTH_GT_8

`define PAGE_PLANE_BLOCK_WIDTH_LTE_8
//`define PAGE_PLANE_BLOCK_WIDTH_GT_8

`define PAGE_PLANE_BLOCK_WIDTH_LTE_16
//`define PAGE_PLANE_BLOCK_WIDTH_GT_16

`define PAGE_PLANE_BLOCK_LUN_WIDTH_LTE_8
//`define PAGE_PLANE_BLOCK_LUN_WIDTH_GT_8

`define PAGE_PLANE_BLOCK_LUN_WIDTH_LTE_16
//`define PAGE_PLANE_BLOCK_LUN_WIDTH_GT_16
