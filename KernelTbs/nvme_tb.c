/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
--
-- Copyright (c) 2013,2014  Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

////////////////////////////////////////////////////////////////////////////////
// Name of the Module	: NVM Express kernel testbench
// Coded by				: M S Santosh Kumar
//
// Module Description	: This is a linux based host side testbench to test data
// 					      transfer to Nvm Express
//
//							Functionality at a glance
//								1. Basic Nvme admin queue configuration
//								2. Dynamic queue allocation
//								3. Multi-page transfer
//								5. Completion status checks
//								6. Error Logs
//								7. Plug-n-play capable linux kernel module
// References			: NVM Express Specification Revision 1.1a
////////////////////////////////////////////////////////////////////////////////


#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/pci.h>
#include <linux/io.h>
#include <linux/delay.h>
#include <uapi/linux/nvme.h>
#include <linux/types.h>

MODULE_LICENSE ("GPL");

#define NVME_VENDOR_ID 0x10EE
#define NVME_DEVICE_ID 0x7024

#define SQ_SIZE(depth)    (depth * sizeof(struct nvme_command))
#define CQ_SIZE(depth)    (depth * sizeof(struct nvme_completion))

enum {
	NVME_CC_ENABLE		= 1 << 0,
	NVME_CC_CSS_NVM		= 0 << 4,
	NVME_CC_MPS_SHIFT	= 7,
	NVME_CC_ARB_RR		= 0 << 11,
	NVME_CC_ARB_WRRU	= 1 << 11,
	NVME_CC_ARB_VS		= 7 << 11,
	NVME_CC_SHN_NONE	= 0 << 14,
	NVME_CC_SHN_NORMAL	= 1 << 14,
	NVME_CC_SHN_ABRUPT	= 2 << 14,
	NVME_CC_SHN_MASK	= 3 << 14,
	NVME_CC_IOSQES		= 6 << 16,
	NVME_CC_IOCQES		= 4 << 20,
	NVME_CSTS_RDY		= 1 << 0,
	NVME_CSTS_CFS		= 1 << 1,
	NVME_CSTS_SHST_NORMAL	= 0 << 2,
	NVME_CSTS_SHST_OCCUR	= 1 << 2,
	NVME_CSTS_SHST_CMPLT	= 2 << 2,
	NVME_CSTS_SHST_MASK	= 3 << 2,
};

static struct pci_device_id nvme_id_table[] = {
  { NVME_VENDOR_ID, NVME_DEVICE_ID, PCI_ANY_ID, PCI_ANY_ID,0,0,0},
  {}
};

struct nvme_dev {
  struct pci_dev *pci_dev;
  struct nvme_bar __iomem *bar;
  unsigned queue_count;
  struct nvme_queue *admin_q;
  u32 __iomem *dbs;
};

struct nvme_bar {
  __u32			cap_msw;	/* Controller Capabilities msw */
  __u32                 cap_lsw;        /* Controller Capabilities lsw */
  __u32			vs;	        /* Version */
  __u32			intms;	        /* Interrupt Mask Set */
  __u32			intmc;	        /* Interrupt Mask Clear */
  __u32			cc; 	        /* Controller Configuration */
  __u32			rsvd1;	        /* Reserved */
  __u32			csts;	        /* Controller Status */
  __u32			rsvd2;	        /* Reserved */
  __u32			aqa;	        /* Admin Queue Attributes */
  __u64			asq;	        /* Admin SQ Base Address */
  __u64			acq;	        /* Admin CQ Base Address */
  __u32                 debug_opcode;   /* Received opcode */
  __u32                 dbg_cmd_id;     /* Received cmd id */
  __u32                 dbg_prp1;       /* Received prp1 */
  __u32                 dbg_prp2       /* Received prp2 */
};

struct nvme_queue {
  dma_addr_t sq_dma_addr;
  dma_addr_t cq_dma_addr;
  struct nvme_command *sq_cmds;
  volatile struct nvme_completion *cqes;
  u16 q_depth;
  u16 sq_head;
  u16 sq_tail;
  u16 cq_head;
  u16 cq_tail;
  u8 cq_phase;
  u16 q_id;
};


static int nvme_dev_map (struct nvme_dev *dev)
{
  int bars, result = 0;
  struct pci_dev *pdev = dev->pci_dev;

  if (pci_enable_device_mem(pdev)) {
    printk(KERN_INFO "Nvmetb: unable to enable Pcie device\n");
    return result;
  }
  else {
    printk(KERN_INFO "NvmeTb: enabled Pcie device\n");
  }
      
  
  pci_set_master(pdev);
  bars = pci_select_bars(pdev, IORESOURCE_MEM);
  
  if (pci_request_selected_regions(pdev,bars,"nvme")) {
	printk(KERN_INFO "Nvmetb: unable to request pci bars\n");
	result = -ENOMEM;
	goto disable_pci;
  }
  
  if (dma_set_mask_and_coherent(&pdev->dev, DMA_BIT_MASK(32))) {
	printk (KERN_INFO "Nvmetb: unable to set dma bit mask to 32");
	result = -ENOMEM;
	goto disable;
  }
  
  dev->bar = ioremap(pci_resource_start(pdev ,0), 8192);
  if (!dev->bar) {
	printk (KERN_INFO "Nvmetb: unable to ioremap pci bar space");
	result = -ENOMEM;
	goto disable;
  }
  printk (KERN_INFO "Nvmetb: controller_status: %x\n",readl(&dev->bar->csts));
  if (readl(&dev->bar->csts) == -1) {
	printk (KERN_INFO "Nvmetb: unable to read from device..device not found");
	result = -ENODEV;
	goto unmap;
  }
  
  dev->dbs = ((void __iomem *)dev->bar) + 4096;

  printk (KERN_INFO "NvmeTb: proper return from map\n");
  return 0;

 unmap:
  iounmap(dev->bar);
  dev->bar = NULL;

 disable:
  pci_release_regions(pdev);
 
 disable_pci:
  pci_disable_device(pdev);
  return result;

}

static int nvme_disable_ctrl(struct nvme_dev *dev)
{
 
  u32 cc = readl(&dev->bar->cc);
  int i = 0;

  if (cc & NVME_CC_ENABLE) {
    writel(cc & ~NVME_CC_ENABLE, &dev->bar->cc);
    printk(KERN_INFO "NvmeTb: Disabled controller\n");
  }
  
  while (((readl(&dev->bar->csts) & NVME_CSTS_RDY) != 0) && i++ != 10) {
    msleep(100);
  }
  
  // timeout condition
  if (i == 11) {
    printk (KERN_INFO "NvmeTb: cntrl status not disabled on request.Timeout\n");
    return -ENODEV;
  }

  return 0;
};

static int nvme_enable_ctrl(struct nvme_dev *dev)
{
  
  u32 cc = readl(&dev->bar->cc);
  int i = 0;
  
  if ((cc & NVME_CC_ENABLE) == 0) {
    writel(cc | NVME_CC_ENABLE, &dev->bar->cc);
    printk(KERN_INFO "NvmeTb: Enabled controller\n");
  }
  
  while ((readl(&dev->bar->csts) & NVME_CSTS_RDY) == 0 && i++ != 10) {
    msleep(100);
  }

  //timeout condition
  if (i == 11) {
    printk (KERN_INFO "NvmeTb: cntrl status not enabled on request. Timeout\n");
    return -ENODEV;
  }
  
  return 0;
}

static struct nvme_queue *nvme_alloc_queue(struct nvme_dev *dev, int qid,
					   int depth)
{
  struct device *dmadev = &dev->pci_dev->dev;
  struct nvme_queue *nvmeq = kzalloc(sizeof(*nvmeq), GFP_KERNEL);

  if (!nvmeq) {
    printk(KERN_INFO "NvmeTb: unable to allocate nvmeq GFP_KERNEL\n");
    return NULL;
  }
  
  // create completion queue
  printk (KERN_INFO "NvmeTb: trying to allocate completion queue of size %x\n",
	  CQ_SIZE(depth));
  nvmeq->cqes = dma_alloc_coherent(dmadev, CQ_SIZE(depth),
				   &nvmeq->cq_dma_addr, GFP_KERNEL);
  
  if (!nvmeq->cqes) {
    goto free_nvmeq;
    printk(KERN_INFO "NvmeTb: completion queue allocation failed 1\n");
    return NULL;
    //TODO TODO free memory handle
  }

  // set the completion queue memory to zeros
  printk(KERN_INFO "NvmeTb: setting completion queue memory to zero\n");
  memset ((void*) nvmeq->cqes, 0, CQ_SIZE(depth));
  
  // create sumbission queue 
  nvmeq->sq_cmds = dma_alloc_coherent(dmadev, SQ_SIZE(depth),
				      &nvmeq->sq_dma_addr,GFP_KERNEL);

  if (!nvmeq->sq_cmds) {
    goto free_cqdma;
    printk (KERN_INFO "NvmeTb: submission queue allocation failed\n");
    return NULL;
  }
  
  nvmeq->cq_head = 0;
  nvmeq->cq_phase = 1;
  nvmeq->q_id = qid;
  nvmeq->q_depth = depth;
  dev->queue_count++;

  return nvmeq;

 free_sqdma:
  dma_free_coherent(dmadev, SQ_SIZE(depth), (void *)nvmeq->sq_cmds,
		    nvmeq->sq_dma_addr);
 free_cqdma:
  dma_free_coherent(dmadev, CQ_SIZE(depth), (void *)nvmeq->cqes,
		    nvmeq->cq_dma_addr);

 free_nvmeq:
  kfree(nvmeq);
  return NULL;
}

static void nvme_dealloc_queue (struct nvme_dev *dev, 
				struct nvme_queue *nvmeq, int depth) {
  
  struct device *dmadev = &dev->pci_dev->dev;
  if (nvmeq && nvmeq->sq_cmds)
    dma_free_coherent(dmadev, SQ_SIZE(depth), (void *)nvmeq->sq_cmds,
		    nvmeq->sq_dma_addr);
  if (nvmeq && nvmeq->cqes)
    dma_free_coherent(dmadev, CQ_SIZE(depth), (void *)nvmeq->cqes,
		      nvmeq->cq_dma_addr);
  if (nvmeq)
    kfree(nvmeq);

} 

static void nvme_deconfigure_admin_queue(struct nvme_dev *dev) {
 
  if (dev->admin_q)
    nvme_dealloc_queue (dev, dev->admin_q, 4);
  nvme_disable_ctrl(dev);

}

static void nvme_unmap_dev (struct nvme_dev *dev) {
  if (dev->bar) {
    iounmap(dev->bar);
    dev->bar = NULL;
    pci_release_regions(dev->pci_dev);
  }
  
  if (pci_is_enabled(dev->pci_dev))
    pci_disable_device(dev->pci_dev);
}
  

static int nvme_configure_admin_queue (struct nvme_dev *dev)
{
  int result;
  u32 aqa;
  struct nvme_queue *nvmeq;
  u64 act_sq_dma_addr;
  u64 act_cq_dma_addr;

  result = nvme_disable_ctrl(dev);
  
  if (result)
    return result;
  
  // allocate admin SQ and CQ
  nvmeq = nvme_alloc_queue(dev, 0, 4);
  if (!nvmeq) {
    printk(KERN_INFO "NvmeTb: nvmeq allocation 1 failed\n");
    return -ENOMEM;
  }

  aqa = nvmeq->q_depth - 1;
  aqa |= aqa << 16;

  writel(aqa, &dev->bar->aqa);
  
  // write admin submission queue address
  writeq(nvmeq->sq_dma_addr, &dev->bar->asq);

  // write admin completion queue address
  writeq(nvmeq->cq_dma_addr, &dev->bar->acq);

  result = nvme_enable_ctrl(dev);
  if (result)
    return result;

  act_sq_dma_addr = readq(&dev->bar->asq);
  act_cq_dma_addr = readq(&dev->bar->acq);

  if ((nvmeq->sq_dma_addr) != act_sq_dma_addr) {
    printk(KERN_INFO "NvmeTb: admin SQ address write failed\n");
    printk(KERN_INFO "NvmeTb: expected: %x, actual: %x\n",nvmeq->sq_dma_addr,
	   act_sq_dma_addr);
  }

  if ((nvmeq->cq_dma_addr) != act_cq_dma_addr) {
    printk(KERN_INFO "NvmeTb: admin CQ address write failed\n");
    printk(KERN_INFO "NvmeTb: expected: %x, actual: %x\n",nvmeq->cq_dma_addr,
	   act_cq_dma_addr);
  }
  
  dev->admin_q = nvmeq;
 
  return 0;
};
  
static int nvme_submit_cmd (struct nvme_dev *dev, struct nvme_queue *queue, struct nvme_command *cmd)
{

  if (queue->sq_head == (queue->sq_tail + 1) % queue->q_depth) {
    printk(KERN_INFO "NvmeTb: submission queue is full\n");
    return 0; //error
  }
  
  printk (KERN_INFO "NvmeTb: sq_tail : %d sq_depth : %d\n", queue->sq_tail, queue->q_depth);
  memcpy(&queue->sq_cmds[queue->sq_tail], cmd, sizeof(*cmd));
  writel(queue->sq_tail + 1, &dev->dbs[queue->q_id * 2]);
  printk (KERN_INFO "NvmeTb: dev->dbs %x qid: %x\n",dev->dbs,queue->q_id);
  printk (KERN_INFO "NvmeTb: sqhdl expected: %x sqhdl read: %x write_addr: %x\n",
	  queue->sq_tail + 1,readl(&dev->dbs[queue->q_id * 2]), &dev->dbs[queue->q_id * 2]);

  printk (KERN_INFO "NvmeTb: submitted opcode: %x\n",readl(&dev->bar->debug_opcode));
  printk (KERN_INFO "NvmeTb: submitted prp1: %x\n",readl(&dev->bar->dbg_prp1));
  
  queue->sq_tail = (queue->sq_tail + 1) % queue->q_depth;
  printk(KERN_INFO "NvmeTb: enqueued submission queue command\n");
  return 0;
}
  
static void print_compl (struct nvme_completion nvme_compl) {
  
  printk (KERN_INFO "\t completion_result: %x\n",nvme_compl.result);
  printk (KERN_INFO "\t completion_sq_head: %x\n", nvme_compl.sq_head);
  printk (KERN_INFO "\t completion sq_id: %x\n", nvme_compl.sq_id);
  printk (KERN_INFO "\t completion command_id: %x\n", nvme_compl.command_id);
  printk (KERN_INFO "\t completion status: %x\n",nvme_compl.status);

}

static int nvme_mkrw_cmd (struct device *dmadev, struct nvme_command **cmd_ptr, 
			  __le64 *prp_list, dma_addr_t prp_dma, int nPages,
			  struct nvme_queue *nvmeq, bool rw)
{
  int i = 0;
  struct nvme_command *cmd;
  *cmd_ptr = kzalloc(sizeof(**cmd_ptr), GFP_KERNEL);
  cmd = *cmd_ptr;

  if (!cmd) {
    printk (KERN_INFO "NvmeTb: unable to allocate space for cmd struct\n");
    return -ENOMEM;
  }
  if (rw)
    cmd->rw.opcode = nvme_cmd_write;
  else
    cmd->rw.opcode = nvme_cmd_read;
  
  cmd->rw.command_id = nvmeq->sq_tail - nvmeq->sq_head;
  cmd->rw.slba = 0;
  cmd->rw.length = nPages - 1;
  
  if (nPages == 1) {
    cmd->rw.prp1 = cpu_to_le64(prp_list[0]);
  }
  else if (nPages == 2) {
    cmd->rw.prp1 = cpu_to_le64(prp_list[0]);
    cmd->rw.prp2 = cpu_to_le64(prp_list[1]);
  }
  else {
    // prp_dma + 8 gives the address of second prp entry
    cmd->rw.prp1 = cpu_to_le64(prp_list[0]);
    cmd->rw.prp2 = cpu_to_le64(prp_dma + 8);
  }
  printk(KERN_INFO "prp1 address %x prp2 address %x\n", cmd->rw.prp1, cmd->rw.prp2);
  return 0;
}

static int nvme_wait_cmpl (struct nvme_queue *nvmeq)
{
  int i = 0;
  while (((nvmeq->cqes[nvmeq->cq_head].status & 0x1) != 
	    (nvmeq->cq_phase & 0x1)) && i++ < 10) {
    msleep(100);
  }
  if (i == 11) {
    printk (KERN_INFO "NvmeTb: no completion received\n");
    return -1;
  }
  return 0;
}

static int nvme_check_dts (struct nvme_dev *dev, struct nvme_queue *nvmeq,
			   int nPages)
{
  struct dma_pool *pool;
  int result = 0;
  struct device *dmadev = &dev->pci_dev->dev;
  __le64 *rbuf_prp_list;
  __le64 *sbuf_prp_list;
  dma_addr_t rbuf_prp_dma;
  dma_addr_t sbuf_prp_dma;

  __le32 **rbuf_prp;
  __le32 **sbuf_prp;

  struct nvme_command *wr_cmd;
  struct nvme_command *rd_cmd;
  unsigned int i;
  unsigned int j;
  bool dts_success = 1;
  
  // create dma pool
  pool = dma_pool_create("prp list page", dmadev, 4096, 4096, 0);
  
  // allocate send and receive buffers

  printk (KERN_INFO "NvmeTb: allocating memory for receive and send buffers\n");
  rbuf_prp = (__le32**) kmalloc(nPages*sizeof(*rbuf_prp), GFP_KERNEL);
  if (!rbuf_prp) {
    printk (KERN_INFO "NvmeTb: receive buffer mem alloc failed\n");
    return -1;
  }

  sbuf_prp = (__le32**) kmalloc(nPages*sizeof(*sbuf_prp), GFP_KERNEL);
  if (!sbuf_prp) {
    printk (KERN_INFO "NvmeTb: send buffer mem alloc failed\n");
    result = -1;
    goto free_rbuf_prp;
  }

  printk (KERN_INFO "allocating memory for prp list\n");
  rbuf_prp_list = (__le64*) dma_pool_alloc (pool, GFP_KERNEL, &rbuf_prp_dma);
  if (!rbuf_prp_list) {
    printk (KERN_INFO "Nvmetb: receive prp list pool allocation failed\n");
    result = -1;
    goto free_sbuf_prp;
  }

  sbuf_prp_list = (__le64*) dma_pool_alloc (pool, GFP_KERNEL, &sbuf_prp_dma);
  if (!sbuf_prp_list) {
    printk (KERN_INFO "NvmeTb: send prp list pool alloc failed\n");
    result = -1;
    goto free_rbuf_prp_list;
  }
  
  for ( i = 0; i < nPages; i++) {
    rbuf_prp[i] = (__le32*) dma_pool_alloc (pool, GFP_KERNEL, &rbuf_prp_list[i]);
    sbuf_prp[i] = (__le32*) dma_pool_alloc (pool, GFP_KERNEL, &sbuf_prp_list[i]);
    printk (KERN_INFO "NvmeTb: index : %d rbuf_prp_address : %x sbuf_prp_address : %x\n",
          i, rbuf_prp_list[i], sbuf_prp_list[i]);
     printk (KERN_INFO "NvmeTb: index : %d rbuf_prp_i_addr : %x sbuf_prp_i_addr : %x\n",
    	    i , &rbuf_prp[i], &sbuf_prp[i]);
     printk (KERN_INFO "NvmeTb: index : %d rbuf_prp_virt_addr : %x sbuf_prp_virt_address : %x\n",
    	    i, rbuf_prp[i], sbuf_prp[i]);
    if (!rbuf_prp[i] || !sbuf_prp[i]) {
      printk(KERN_INFO "NvmeTb: rbuf or sbuf pool allocation failed\n");
      result = -1;
      // free all allocated pools
      
      if (!rbuf_prp[i])
	dma_pool_free (pool, rbuf_prp[i], rbuf_prp_list[i]);
      if (!sbuf_prp[i])
	dma_pool_free (pool, sbuf_prp[i], sbuf_prp_list[i]);
      for (j = 0; j < i; j++) {
	dma_pool_free (pool, rbuf_prp[j], rbuf_prp_list[j]);
	dma_pool_free (pool, sbuf_prp[j], sbuf_prp_list[j]);
      }
      
      // need to change this .. bad prog 
      goto free_sbuf_prp_list;
    }
  }
  
  printk (KERN_INFO "NvmeTb: initiating data in send buffer\n");
  // write random data to sbuf
  for (i = 0; i < nPages; i++) {
    for (j = 0; j < 1024; j++) {
      sbuf_prp[i][j] = (i<<10) + j;
    }
  }

  // make write command
  printk (KERN_INFO "NvmeTb: creating write to nand flash command\n");
  result = nvme_mkrw_cmd (dmadev, &wr_cmd, sbuf_prp_list, sbuf_prp_dma, nPages, nvmeq, 1);
  if (result) {
    printk (KERN_INFO "NvmeTb: nvme_mkrw_cmd creation failed\n");
    goto free_dma_pools;
  }

  printk (KERN_INFO "NvmeTb: Submitting write command to nvme\n");
  nvme_submit_cmd (dev, nvmeq, wr_cmd);
  
  // wait for completion
  result = nvme_wait_cmpl(nvmeq);
  if (result) {
    printk (KERN_INFO "NvmeTb: write command submitted,no completion received\n");
    goto free_wr_cmd;
  }

  printk (KERN_INFO "NvmeTb: write command completion received\n");
  
  print_compl(nvmeq->cqes[nvmeq->cq_head]);
  nvmeq->cq_head++;
  // msleep(1000);

  printk (KERN_INFO "NvmeTb: Printing receive and send buffer data for debug\n");
  for (i = 0; i < nPages; i++) {
    for (j = 0; j < 10; j++) {
      printk (KERN_INFO "Nvmetb: index: %d %d sbuf_data: %x sbuf_addr: %x rbuf_data: %x rbuf_addr: %x\n",
	      i,j,sbuf_prp[i][j],&sbuf_prp[i][j],rbuf_prp[i][j], &rbuf_prp[i][j]);	
	}
    }

  // make read command
  printk (KERN_INFO "NvmeTb: creating read from nand flash command\n");
  result = nvme_mkrw_cmd (dmadev, &rd_cmd, rbuf_prp_list, rbuf_prp_dma , nPages, nvmeq, 0);
  if (result) {
    printk (KERN_INFO "NvmeTb: nvme_mkrw_command_read failed\n");
    goto free_wr_cmd;
  }
  
  printk (KERN_INFO "NvmeTb: Submitting read command to nvme\n");
  nvme_submit_cmd (dev, nvmeq, rd_cmd);
  result = nvme_wait_cmpl(nvmeq);
  if (result) {
    printk (KERN_INFO "NvmeTb: read command submitted, no completion received\n");
    goto free_rd_cmd;
  }
  
  printk (KERN_INFO "NvmeTb: read command completion received\n");
  print_compl(nvmeq->cqes[nvmeq->cq_head]);
  nvmeq->cq_head++;
  msleep(1000);

  printk (KERN_INFO "NvmeTb: Checking status of data transfer\n");
  // check data transmission
  for (i = 0; i < nPages; i++) {
    for (j = 0; j < 1024; j++) {
      if (sbuf_prp[i][j] != rbuf_prp[i][j]) {
	printk (KERN_INFO "Nvmetb: index: %d %d sbuf_data %x rbuf_data %x\n",
		i,j,sbuf_prp[i][j],rbuf_prp[i][j]);	
	dts_success = 0;
	}
    }
  }
  if (dts_success) {
    printk (KERN_INFO "NvmeTb: Data transfer test for %d pages successful\n", nPages);
    result = 0;
  }
  else {
    printk (KERN_INFO "NvmeTb: Printed ismatched data between send and receive buffers\n");
    printk (KERN_INFO "NvmeTb: Data transfer test for %d pages failed\n", nPages);
    result = -1;
  }

 free_rd_cmd:
  kfree(rd_cmd);

 free_wr_cmd:
  kfree(wr_cmd);

 free_dma_pools:
  printk (KERN_INFO "NvmeTb: freeing dma pools \n");
  for (i = 0; i < nPages; i++) {
    dma_pool_free (pool, rbuf_prp[i], rbuf_prp_list[i]);
    dma_pool_free (pool, sbuf_prp[i], sbuf_prp_list[i]);
  }

 free_sbuf_prp_list:
  dma_pool_free (pool, sbuf_prp_list, sbuf_prp_dma);

 free_rbuf_prp_list:
  dma_pool_free (pool, rbuf_prp_list, rbuf_prp_dma);

 free_sbuf_prp: 
  kfree(sbuf_prp);
  
 free_rbuf_prp:
  kfree(rbuf_prp);
  printk (KERN_INFO "NvmeTb: data transfer test complete\n");
  return result;

}

static int nvme_probe (struct pci_dev *pdev, const struct pci_device_id *id)
{
  int result = -ENOMEM;
  struct nvme_dev *dev;
  struct nvme_command *create_sq_cmd;
  struct nvme_command *create_cq_cmd;
  struct nvme_queue *admin_q;
  struct nvme_queue *nvmeq_1;
  int i = 0;
  
  dev = kzalloc(sizeof(*dev), GFP_KERNEL);
  if (!dev) {
    printk ("NvmeTb: unable to allocate device struct\n");
    return 0;
  }
  dev->pci_dev = pdev;

  printk (KERN_INFO "NvmeTb: mapping nvme device \n");
  result = nvme_dev_map(dev);
  
  if (result) {
    printk (KERN_INFO "Nvmetb: dev map failed\n");
    goto free_dev;
  }
  else
    printk (KERN_INFO "NvmeTb: device map successfull\n");
  
  printk (KERN_INFO "NvmeTb: Configuring admin queue \n");
  result = nvme_configure_admin_queue(dev);
  if (result) {
    printk (KERN_INFO "NvmeTb: configuring admin queue failed\n");
    goto unmap_dev;
  }
  else
    printk (KERN_INFO "NvmeTb: admin queue configuration successful\n");
  
  admin_q = dev->admin_q;

  // create first completion queue

  printk (KERN_INFO "NvmeTb: allocating memory for first nvme queue\n");
  nvmeq_1 = nvme_alloc_queue(dev,dev->queue_count,4);
  if (!nvmeq_1) {
    printk (KERN_INFO "NvmeTb: nvme queue allocation failed\n");
    goto nvme_deconfigure_admin_queue;
  }
  else
    printk(KERN_INFO "NvmeTb: memory allocation for nvme queue successful\n");

  create_cq_cmd = kmalloc(sizeof(*create_cq_cmd), GFP_KERNEL);

  printk(KERN_INFO "NvmeTb: completion status before submission : %x\n",
	 admin_q->cqes[admin_q->cq_head].status);
  
  if (!create_cq_cmd) {
    printk (KERN_INFO "NvmeTb: memory alloc for create_cq failed\n");
    goto dealloc_nvmeq_1;
  }
  
  create_cq_cmd->create_cq.opcode = nvme_admin_create_cq;
  create_cq_cmd->create_cq.flags = 0;
  create_cq_cmd->create_cq.command_id = 
    (nvmeq_1->sq_tail - nvmeq_1->sq_head);
  create_cq_cmd->create_cq.prp1 = cpu_to_le64(nvmeq_1->cq_dma_addr);
  create_cq_cmd->create_cq.cqid = nvmeq_1->q_id;
  create_cq_cmd->create_cq.qsize = nvmeq_1->q_depth;
  create_cq_cmd->create_cq.cq_flags = 0;
  create_cq_cmd->create_cq.irq_vector = nvmeq_1->q_id;
  
  printk (KERN_INFO "NvmeTb: Submitting admin command for creating completion queue\n");
  nvme_submit_cmd(dev, admin_q, create_cq_cmd);

  i = 0;
  printk (KERN_INFO "\tNvmeTb: expected opcode %x\n",create_cq_cmd->create_cq.opcode);
  printk (KERN_INFO "\tNvmeTb: expected prp1 %x\n", create_cq_cmd->create_cq.prp1);
  printk(KERN_INFO "\tNvmeTb: expected phase tag: %x\n",admin_q->cq_phase);
  printk(KERN_INFO "\tNvmeTb: expected commandId: %x\n",create_cq_cmd->create_cq.command_id);
  while ((((admin_q->cqes[admin_q->cq_head]).status & 0x1) != 
	 (admin_q->cq_phase & 0x1)) && i++ < 10) {
    msleep(100);
  }

  if ( i == 11) {
    printk(KERN_INFO "NvmeTb: No valid completion received for create_cq command\n");
    return 0; //error
  }
  else {
    printk(KERN_INFO "NvmeTb: completion for create_cq command received\n");
    print_compl(admin_q->cqes[admin_q->cq_head]);
  }
 
  // TODO completion queue is assumed to be not full 
  admin_q->cq_head = admin_q->cq_head + 1;
  
  // create first submission queue
  
  create_sq_cmd = kmalloc(sizeof(*create_sq_cmd), GFP_KERNEL);
  
  if (!create_cq_cmd) {
    printk (KERN_INFO "NvmeTb: memory alloc for create_sq failed\n");
    goto free_create_cq_cmd;
  }
  
  create_sq_cmd->create_sq.opcode = nvme_admin_create_sq;
  create_sq_cmd->create_sq.flags = 0;
  create_sq_cmd->create_sq.command_id = 
    (nvmeq_1->sq_tail - nvmeq_1->sq_head);
  create_sq_cmd->create_sq.prp1 = cpu_to_le64(nvmeq_1->sq_dma_addr);
  create_sq_cmd->create_sq.sqid = nvmeq_1->q_id;
  create_sq_cmd->create_sq.qsize = nvmeq_1->q_depth;
  create_sq_cmd->create_sq.sq_flags = 0;
  create_sq_cmd->create_sq.cqid = nvmeq_1->q_id;

  printk (KERN_INFO "Submitting admin command for creating submission queue\n");
  nvme_submit_cmd(dev, admin_q, create_sq_cmd);

  i = 0;
  while ((((admin_q->cqes[admin_q->cq_head]).status & 0x1) != 
	  admin_q->cq_phase & 0x1) && i++ < 10) {
    msleep(100);
  }
  
  if (i == 11) {
    printk (KERN_INFO "NvmeTb: No valid completion received for create_sq command\n");
    return 0; //error
  }
  else {
    printk (KERN_INFO "NvmeTb: completion for create_sq command received\n");
    print_compl(admin_q->cqes[admin_q->cq_head]);
  }
  
  admin_q->cq_head++;

  // data transfer check
  printk (KERN_INFO "NvmeTb: Initiating data transfer check\n");
  result = nvme_check_dts (dev, nvmeq_1, 4);

  // free submission queue
  printk (KERN_INFO "NvmeTb: freeing create_sq_cmd\n");
  kfree (create_sq_cmd);
  
 free_create_cq_cmd : 
  printk (KERN_INFO "NvmeTb: freeing create_cq_cmd\n");
  kfree (create_cq_cmd);

 dealloc_nvmeq_1 :
  printk (KERN_INFO "NvmeTb: deallocating nvmeq_1\n");
  nvme_dealloc_queue(dev,nvmeq_1,4);

 nvme_deconfigure_admin_queue :
  printk (KERN_INFO "NvmeTb: deconfiguring admin queue\n");
  nvme_deconfigure_admin_queue(dev);

 unmap_dev:
  printk (KERN_INFO "NvmeTb: unmapping nvme dev\n");
  nvme_unmap_dev(dev);

 free_dev:
  printk (KERN_INFO "NvmeTb: freeing dev\n");
  kfree(dev);

  return 0;
}
  
void nvme_remove (struct pci_dev *pdev)
{
  printk(KERN_INFO "Nvmetb: nvme removed\n");  
  pci_release_regions(pdev);
  pci_disable_device(pdev);
}

struct pci_driver nvme_driver = {
  
 name: "Nvmetb",
 id_table: nvme_id_table,
 probe: nvme_probe,
 remove: nvme_remove

};


static int nvme_init(void)
{
  printk (KERN_INFO "Nvmetb: Init\n");
  return pci_register_driver(&nvme_driver);
}

void nvme_clean_up(void)
{
  printk (KERN_INFO "Nvmetb: cleanup\n");
  pci_unregister_driver(&nvme_driver);

}

module_init(nvme_init);
module_exit(nvme_clean_up);
